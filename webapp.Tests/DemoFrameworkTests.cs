﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace framework.Tests
{
    [TestClass()]
    public class DemoFrameworkTests
    {
        [TestMethod()]
        public void DemoBoolMethodTest()
        {

            Assert.IsFalse( DemoFramework.DemoBoolMethod() );
        }

        [TestMethod()]
        public void DemoBoolMethod2Test()
        {
            Assert.IsFalse(DemoFramework.DemoBoolMethod2(true));
        }
    }
}
