﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace framework
{
    public class DemoFramework
    {

        public static bool DemoBoolMethod()
        {
            return true;
        }

        public static  bool DemoBoolMethod2( bool returnOppositeValue)
        {
            return !returnOppositeValue;
        }
    }
}
